# Cryptdir
Encrypt and decrypt directories with portable standalone program

Supported platform: Linux and Windows. (Mac is not tested.)

## Motivation
Many file encryption programs require installation, a pile of dependencies, and super-user privileges. This program is made to solve that. It's portable, practical, and dead simple.

This program is uses command line user interface, which has two commands
* ```cryptdir close <directory> <vault>```
    * convert directory to encrypted vault
* ```cryptdir open <vault>```  
    * convert encrypted vault back to directory

Binary executable is depedency-free and portable, so it can be always saved along with the encrypted vault. If vault and binary are stored next to each other, then vault can be **always** opened on **any** machine, as long as binary matches the operating system (linux/windows) and architecture (x86-64/arm). No more situations *"Oh shit, I have this 4 year old encrypted vault, and I don't remember how to open it"*. To open the vault just run the binary and write the password. If the vault is 4 years old, then remebering password is the only concern.

## Getting binary
### Pre-built binary
For x86 64bit system with Linux or Windows, there exists pre-built binaries in `binaries` directory. For other systems compilation is required.

### Compilation
* Clone this repo `git clone https://gitlab.com/tolvanea/cryptdir.git`
* [Install rust](https://rustup.rs/), and run in cloned directory ```cargo build --release```
* Grab generated binary from ```target/release/```

## Usage

### Encrypt and decrypt
In this example directory is named `my_directory`, and the binary of this program is named `cryptdir`. They both are placed in same directory, in which shell terminal commands are run. Encrypt `my_directory` to vault named `encrypted_vault` with

```$ ./cryptdir close my_directory encrypted_vault```

The program will ask to define password for vault. When this is done, `my_directory` is removed and `encrypted_vault` is left.

Decrypt vault back to original directory with:

```$ ./cryptdir open encrypted_vault```

After typing in the password, original directory is restored, but the program is left running. This is because modified directory can now be encrypted again just by writing `close`. There is no need to retype password.

### Shorthand: Use default directory name `vault`
Name arguments can be omitted, if the directory is named as `vault`. Then encryption can be made just with

```$ ./cryptdir close```

This will create encrypted vault named `encrypted_vault`. This vault can be opened with

```$ ./cryptdir open```


### Help / information
Help

```$ ./cryptdir --help```

Some information about the program

```$ ./cryptdir --about```

    
## Security concerns

* Encrypted vault is extracted as a normal directory on hard drive. Directory content may be recoverable with advanced disk tools. If you want to prevent file recovery, open vault on RAM-disk.

* The author is far from cryptography expert, and this program is not audited in any way. However, all cryptography is provided by external libraries, which are quite reliable. These libraries are widely adapted in rust community, but none of them are officially audited by a third party security company. So this program is bundles few existing tools together with simple user interface.




## Side notes

* Encryption is done with AES. If your hardware supports optimized AES-NI instructions, they will be selected at the runtime instead of slower software implementation.

    * Directory of size 1 GB takes few seconds to encrypt and decrypt. However, without AES-instruction support, the same process takes half a minute.

* Encryption of large directories is not well supported. The directory must fit in RAM in one piece. So directories should be kept small (< 1 GB).
