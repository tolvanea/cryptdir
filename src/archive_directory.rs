use tar::{Archive, Builder};
use std::{path::Path, io::{Cursor, Error}};

/// Read directory from hard drive, archive it with tar, and return it in bytes
pub fn archive_dir_to_bytes(src_dir: &Path) -> Result<Vec<u8>, Error> {
    let buff = Cursor::new( Vec::new());
    let mut tar = Builder::new(buff);
    tar.append_dir_all(".", src_dir )?;
    Ok(tar.into_inner()?.into_inner())
}

///  Given tar-archive in bytes, unpack it, and write directory to hard drive
pub fn unpack_archive_from_bytes_to_dir(tar_bytes: &[u8], dst_path: &Path,) -> Result<(), Error> {
    let arhcive_reader = Cursor::new(tar_bytes);
    let mut archive = Archive::new(arhcive_reader);
    archive.unpack(dst_path)?;
    Ok(())
}
