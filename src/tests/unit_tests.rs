use std::io::{Write, Read};
use std::fs::File;
#[allow(unused_imports)]
use std::path::{Path, PathBuf};
use hex_literal::hex;
use tempfile::tempdir;

#[allow(unused_imports)]
use crate::archive_directory::{archive_dir_to_bytes, unpack_archive_from_bytes_to_dir};
#[allow(unused_imports)]
use crate::cryptography::{encrypt_with_passphrase, decrypt_with_passphrase, hash_passphrase_to_bytes};

use super::{create_dir_and_fill_it, read_created_dir};

/// Test archiving directory in tar and unarchiving it
#[test]
fn test_taring_only() {
    pub fn tar_dir(src_dir: &Path, dst_file: &Path) -> Result<(), std::io::Error> {
        let tar_gz = File::create(dst_file)?;
        let mut tar = tar::Builder::new(tar_gz);
        tar.append_dir_all(".", src_dir)?;
        tar.into_inner()?;
        Ok(())
    }

    pub fn untar_dir(dst_path: &Path, uncompressed_path: &Path,) -> Result<(), std::io::Error> {
        let tar_gz = File::open(dst_path)?;
        let mut archive = tar::Archive::new(tar_gz);
        archive.unpack(uncompressed_path)?;
        Ok(())
    }

    let dir = tempdir().unwrap();
    let compressed = tempdir().unwrap();
    let compressed_path = compressed.path().join("test.tar");
    let uncompressed = tempdir().unwrap();

    create_dir_and_fill_it(dir.path());
    tar_dir(dir.path(), &compressed_path).unwrap();
    untar_dir(&compressed_path, uncompressed.path()).unwrap();

    read_created_dir(uncompressed.path());
}

/// Test encrypting and decrypting
#[test]
fn test_encryption() {
    use aes_soft as aes;

    use block_modes::{BlockMode, Cbc};
    use block_modes::block_padding::Pkcs7;
    use aes::Aes128;
    type Aes128Cbc = Cbc<Aes128, Pkcs7>;

    let key = hex!("000102030405060708090a0b0c0d0e0f");
    let iv = hex!("f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff");
    let bytes = b"This is sectet message";

    // Encrypt
    let cipher = Aes128Cbc::new_var(&key, &iv).unwrap();
    let ciphertext = cipher.encrypt_vec(bytes);

    // Decrypt
    let cipher = Aes128Cbc::new_var(&key, &iv).unwrap();
    let decrypted = cipher.decrypt_vec(&ciphertext).unwrap();

    assert_eq!(decrypted, bytes);
}


/// Try archiving with tar, encrypting, and passphrase to key conversion
#[test]
fn test_taring_and_encryption() {
    let vault = tempdir().unwrap();
    let tmp_dir = tempdir().unwrap();
    let encrypted_path = tmp_dir.path().join("encrypted");
    let decrypted_vault = tempdir().unwrap();

    create_dir_and_fill_it(vault.path());

    // simulate ask_passphrace() with password that is worlds hardest to guess
    let passphrase = "passw0rd1".to_string();

    // archive with tar
    let bytes = archive_dir_to_bytes(vault.path()).unwrap();

    // encrypt
    let encrypted = encrypt_with_passphrase(&passphrase, bytes.as_slice());
    let mut encrypted_file = File::create(&encrypted_path).unwrap();
    encrypted_file.write_all(encrypted.as_slice()).unwrap();
    drop(encrypted_file);

    //Decrypt
    let mut buf = Vec::new();
    let mut encrypted_file = File::open(&encrypted_path).unwrap();
    let _len = encrypted_file.read_to_end(&mut buf).unwrap();
    let clear_data = decrypt_with_passphrase(&passphrase, buf.as_slice()).unwrap();
    unpack_archive_from_bytes_to_dir(
        clear_data.as_slice(),
        decrypted_vault.path()
    ).unwrap();

    read_created_dir(decrypted_vault.path());

    assert_eq!(clear_data, bytes);
}


#[test]
fn test_passphrase_to_crytp_key_conversion() {
    // This is not trivial test: Key stretching must not be randomized as it is
    // by default.
    assert_eq!(
        hash_passphrase_to_bytes("Hello"),
        hash_passphrase_to_bytes("Hello")
    )
}

