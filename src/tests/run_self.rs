/// This module reduces boilerplate on stdin/stdout testing. It implements
/// interface to read and write stdout to the process of program itself.

use std::process::{Command, Stdio, ChildStdout, ChildStdin, ChildStderr, Child};
use std::io::{Read, BufReader, BufWriter, Write, Bytes};
use std::iter::Iterator;
use std::path::Path;
use assert_cmd::cargo::CommandCargoExt;
use std::{thread::sleep, time::Duration};
use anyhow::anyhow;
type Res<T> = anyhow::Result<T>;

pub struct Prog {
    process: Child,
    pub stdin: BufWriter<ChildStdin>,
    pub stdout_bytes: Bytes<BufReader<ChildStdout>>,
    pub stderr_bytes: Bytes<BufReader<ChildStderr>>,
}

impl Prog {
    /// Read characters from stdout
    pub fn stdout(&mut self, s: &str) -> Res<bool> {
        sleep(Duration::from_millis(10));
        let mut bytes = Vec::with_capacity(s.len());
        for b_param in s.bytes() {
            match self.stdout_bytes.next() {
                Some(b_stdout) => {
                    let b_stdout = b_stdout?;
                    bytes.push(b_stdout);
                    if b_param != b_stdout {
                        // Write one more character to bytes that will be printed on screen
                        let mut char_buf = [0_u8; 4];
                        let mut idx = 0;
                        while let Some(b) = self.stdout_bytes.next() {
                            char_buf[idx] = b?;
                            idx += 1;
                            match std::str::from_utf8(&char_buf[0..idx]) {
                                Ok(_) => {
                                    bytes.extend(char_buf[0..idx].iter());
                                    break;
                                },
                                Err(_) => assert!(idx < 4),
                            }
                        }
                        return Err(anyhow!(
                            "Stdout differs from desired input.\n\
                            Desired:\n    '{}'\n\
                            Stdout:\n    '{}'....\n",
                            &s, &String::from_utf8_lossy(&bytes)
                        ))
                    }
                },
                None => {
                    let msg = "Program ended while reading stdout.";
                    let stderr = self.stderr_bytes.by_ref().collect::<Result<Vec<u8>, _>>()?;
                    let stderr = String::from_utf8_lossy(&stderr);
                    let (extra1, extra2) = if stderr.len() > 0 {
                        ("\n However, following text was written to stderr:\n    ", stderr.as_ref())
                    } else {
                        ("", "")
                    };
                    return Err(anyhow!(
                        "{}\nDesired:\n    '{}'\nStdout:\n    '{}'{}{}\n",
                        msg, &s, &String::from_utf8_lossy(&bytes), extra1, extra2
                    ))
                },
            }
        }
        sleep(Duration::from_millis(10));
        return Ok(true);
    }

    pub fn stderr(&mut self, s: &str) -> Res<bool> {
        sleep(Duration::from_millis(10));
        let mut bytes: Vec<u8> = Vec::with_capacity(s.len());
        for b_param in s.bytes() {
            match self.stderr_bytes.next() {
                Some(b_stderr) => {
                    let b_stderr = b_stderr?;
                    bytes.push(b_stderr);
                    if b_param != b_stderr {
                        dbg!((b_param, b_stderr));
                        let mut char_buf = [0_u8; 4];
                        let mut idx = 0;
                        while let Some(b) = self.stderr_bytes.next() {
                            char_buf[idx] = b?;
                            idx += 1;
                            match std::str::from_utf8(&char_buf[0..idx]) {
                                Ok(_) => {
                                    bytes.extend(char_buf[0..idx].iter());
                                    break;
                                },
                                Err(_) => assert!(idx < 4),
                            }
                        }
                        return Err(anyhow!(
                            "Stderr differs from desired input.\n\
                            Desired:\n    '{}'\n\
                            Stderr:\n    '{}'...\n",
                            &s, &String::from_utf8_lossy(&bytes)
                        ))
                    }
                },
                None => {
                    let msg = "Program ended while reading stderr.";
                    let stdout = self.stdout_bytes.by_ref().collect::<Result<Vec<u8>, _>>()?;
                    let stdout = String::from_utf8_lossy(&stdout);
                    let (extra1, extra2) = if stdout.len() > 0 {
                        ("\n However, following text was written to stdout:\n    ", stdout.as_ref())
                    } else {
                        ("", "")
                    };
                    return Err(anyhow!(
                        "{}\nDesired:\n    '{}'\nStderr:\n    '{}'{}{}\n",
                        msg, &s, &String::from_utf8_lossy(&bytes), extra1, extra2
                    ))
                },
            }
        }
        sleep(Duration::from_millis(10));
        return Ok(true);
    }
    
    /// Write one line in stdin
    pub fn write_line(&mut self, s: &str) {
        sleep(Duration::from_millis(10));
        let s = s.to_string() + "\n";
        self.stdin.write_all(s.as_bytes()).unwrap();
        self.stdin.flush().unwrap();
        sleep(Duration::from_millis(10));
    }
    /// Return Some(error code) if process has exited, else return None
    pub fn exit_status(&mut self) -> Option<i32> {
        sleep(Duration::from_millis(50));
        match self.process.try_wait() {
            Ok(Some(s)) => Some(s.code().expect("Process got terminated by a signal!")),
            Ok(None) => None,
            Err(_) => panic!("Something went wrong with the process.")
        }
    }

    // Try for 5 sec to check whether program has exited
    pub fn poll_exit_status(&mut self) -> Option<i32> {
        let mut itr = 0;
        let exit_code = loop {
            sleep(Duration::from_millis(100));
            match self.exit_status() {
                Some(i) => break Some(i),
                None => if itr > 50 { break None },
            }
            itr += 1
        };
        return exit_code
    }

    // For debugging only: Print all stdout and stderr for exited program. If running, then hang.
    #[allow(dead_code)]
    pub fn dump_everything(&mut self) -> Res<bool> {
        // if self.exit_status().is_none() {
        //     return Ok(false);  // Still running
        // }
        let mut char_buf = [0_u8; 4];
        let mut idx = 0;
        while let Some(b) = self.stdout_bytes.next() {
            char_buf[idx] = b?;
            idx += 1;
            match std::str::from_utf8(&char_buf[0..idx]) {
                Ok(s) => {
                    idx = 0;
                    print!("{}", s);
                    std::io::stdout().flush().unwrap();
                },
                Err(_) => assert!(idx < 4),
            }
        }
        while let Some(b) = self.stderr_bytes.next() {
            char_buf[idx] = b?;
            idx += 1;
            match std::str::from_utf8(&char_buf[0..idx]) {
                Ok(s) => {
                    idx = 0;
                    print!("{}", s);
                    std::io::stdout().flush().unwrap();
                },
                Err(_) => assert!(idx < 4),
            }
        }
        return Ok(true);
    }
}

pub fn start_program(args: &[&str], cwd: &Path) -> Prog {
    let mut process = Command::cargo_bin(env!("CARGO_PKG_NAME")).unwrap()
        .args(args)
        .stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .current_dir(cwd)
        .spawn()
        .unwrap();
    let stdin = BufWriter::new(process.stdin.take().unwrap());
    let stdout_bytes = BufReader::new(process.stdout.take().unwrap()).bytes();
    let stderr_bytes = BufReader::new(process.stderr.take().unwrap()).bytes();
    sleep(Duration::from_millis(50));
    return Prog{stdin, stdout_bytes, stderr_bytes, process};
}
