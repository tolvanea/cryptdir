/// ##########################################################################
/// IMPORTANT!!! YOU MUST RUN "cargo build" BEFORE running integration tests!
/// ##########################################################################


/// This file contains integration tests including command line interface input.

use tempfile::tempdir;

use super::{create_dir_and_fill_it, read_created_dir};
use super::run_self::start_program;
use super::super::user_input::{
    ABOUT_TEXT, 
    YOU_SCREVED_INPUT, 
    VAULT_DEFAULT_NAME, 
    ENCRYPTED_DEFAULT_NAME
};

const VAULT_CUSTOM_NAME: &str = "myDiRECt0ry"; // even emojis are supported! 😁
const ENCRYPTED_CUSTOM_NAME: &str = "try_to_open_this_NSA"; // Try them if you dare 😈
const PASSWORD: &str = "pA55wörd!";

/// Create dir, close it, open it, assert content, close it
#[test]
fn cli_test() {
    let tmp_dir = tempdir().unwrap();
    let vault_path = tmp_dir.path().join(VAULT_DEFAULT_NAME);
    let encrypted_path = tmp_dir.path().join(ENCRYPTED_DEFAULT_NAME);

    create_dir_and_fill_it(&vault_path);

    // Close it
    let mut p = start_program(&["close"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Confirm it: ").unwrap());
    p.write_line(PASSWORD);
    assert_eq!(p.poll_exit_status(), Some(0));
    
    assert!(encrypted_path.is_file());
    assert!(!vault_path.is_dir());
    
    // Open it
    let mut p = start_program(&["open"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Close vault by typing 'close'. Alternatively, vault can be left open\n").unwrap());
    assert!(&p.stdout("either by typing 'leave open' or by terminating this program.\n").unwrap());
    assert!(&p.stdout("> ").unwrap());

    // Check opened file
    assert!(vault_path.is_dir());
    assert!(!encrypted_path.is_file());
    read_created_dir(&vault_path);

    // Close
    p.write_line("close");
    assert_eq!(p.poll_exit_status(), Some(0));

    assert!(!vault_path.is_dir());
    assert!(encrypted_path.is_file());
}


/// Create dir with custom names, close it, open it, assert content, and exit
/// without closing it.
#[test]
fn cli_test_with_custom_names() {
    let tmp_dir = tempdir().unwrap();
    let vault_path = tmp_dir.path().join(VAULT_CUSTOM_NAME);
    let encrypted_path = tmp_dir.path().join(ENCRYPTED_CUSTOM_NAME);

    create_dir_and_fill_it(&vault_path);

    // Close it
    let mut p = start_program(&["close", VAULT_CUSTOM_NAME, ENCRYPTED_CUSTOM_NAME], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Confirm it: ").unwrap());
    p.write_line(PASSWORD);
    assert_eq!(p.poll_exit_status(), Some(0));

    assert!(encrypted_path.is_file());
    assert!(!vault_path.is_dir());

    // Open it
    let mut p = start_program(&["open", ENCRYPTED_CUSTOM_NAME], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Close vault by typing 'close'. Alternatively, vault can be left open\n").unwrap());
    assert!(&p.stdout("either by typing 'leave open' or by terminating this program.\n").unwrap());
    assert!(&p.stdout("> ").unwrap());

    // Check opened file
    assert!(vault_path.is_dir());
    assert!(!encrypted_path.is_file());
    read_created_dir(&vault_path);

    // exit
    p.write_line("leave open");
    assert!(&p.stdout("Vault was left open. You can close it by './cryptdir close'\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(0));

    assert!(vault_path.is_dir());
    assert!(!encrypted_path.is_file());
}


/// Try to open vault that does not exist
#[test]
fn try_to_close_nonexistent_vault() {
    let tmp_dir = tempdir().unwrap();

    // Close it (default name)
    let mut p = start_program(&["close"], tmp_dir.path());
    let err_msg = format!(
        "Error: Directory '{}' does not exist in current directory.\n", 
        VAULT_DEFAULT_NAME
    );
    assert!(&p.stderr(&err_msg).unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));

    // Close it, but now with custom name
    let mut p = start_program(&["close", VAULT_CUSTOM_NAME, ENCRYPTED_CUSTOM_NAME], tmp_dir.path());
    let err_msg = format!(
        "Error: Directory '{}' does not exist in current directory.\n", 
        VAULT_CUSTOM_NAME
    );
    assert!(&p.stderr(&err_msg).unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));
}


/// Try to open vault that does not exist
#[test]
fn try_to_open_nonexistent_vault() {
    let tmp_dir = tempdir().unwrap();

    // Open it
    let mut p = start_program(&["open"], tmp_dir.path());
    let err_msg = format!(
        "Error: Encrypted vault with name '{}' not found in current directory.\n", 
        ENCRYPTED_DEFAULT_NAME
    );
    assert!(&p.stderr(&err_msg).unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));

    // Again, but with custom name.
    let mut p = start_program(&["open", ENCRYPTED_CUSTOM_NAME], tmp_dir.path());
    let err_msg = format!(
        "Error: Encrypted vault with name '{}' not found in current directory.\n", 
        ENCRYPTED_CUSTOM_NAME
    );
    assert!(&p.stderr(&err_msg).unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));
}


/// Try to open vault in case where there is already a vault with a same name
#[test]
fn try_to_open_vault_when_it_already_exists() {
    let tmp_dir = tempdir().unwrap();
    let vault_path = tmp_dir.path().join(VAULT_DEFAULT_NAME);
    let encrypted_path = tmp_dir.path().join(ENCRYPTED_DEFAULT_NAME);

    create_dir_and_fill_it(&vault_path);

    // Close it
    let mut p = start_program(&["close"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Confirm it: ").unwrap());
    p.write_line(PASSWORD);
    assert_eq!(p.poll_exit_status(), Some(0));

    assert!(encrypted_path.is_file());
    assert!(!vault_path.is_dir());
    create_dir_and_fill_it(&vault_path);
    assert!(vault_path.is_dir());

    // Open it, but cancel
    let mut p = start_program(&["open"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    let err_msg = format!(
        "Error: Output vault '{}' already exists. Overwrite?\n", 
        VAULT_DEFAULT_NAME
    );
    assert!(&p.stderr(&err_msg).unwrap());
    assert!(&p.stderr("[Y/N] ").unwrap());
    p.write_line("n");
    assert!(&p.stderr("Not opening vault.\n").unwrap());

    // Open it, but overwrite
    let mut p = start_program(&["open"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    let err_msg = format!(
        "Error: Output vault '{}' already exists. Overwrite?", 
        VAULT_DEFAULT_NAME
    );
    assert!(&p.stderr(&err_msg).unwrap());
    p.write_line("y");
    assert!(&p.stdout("Close vault by typing 'close'. Alternatively, vault can be left open\n").unwrap());
    assert!(&p.stdout("either by typing 'leave open' or by terminating this program.\n").unwrap());
    assert!(&p.stdout("> ").unwrap());
    
    // Check opened file
    assert!(vault_path.is_dir());
    assert!(!encrypted_path.is_file());
    read_created_dir(&vault_path);
    
    // exit
    p.write_line("leave open");
    assert!(&p.stdout("Vault was left open. You can close it by './cryptdir close'\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(0));
}


/// Try to open with incorrect password
#[test]
fn try_wrong_passwords() {
    let tmp_dir = tempdir().unwrap();
    let vault_path = tmp_dir.path().join(VAULT_DEFAULT_NAME);
    let encrypted_path = tmp_dir.path().join(ENCRYPTED_DEFAULT_NAME);

    create_dir_and_fill_it(&vault_path);

    // Close it
    let mut p = start_program(&["close"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Confirm it: ").unwrap());
    p.write_line(PASSWORD);
    assert_eq!(p.poll_exit_status(), Some(0));

    assert!(encrypted_path.is_file());
    assert!(!vault_path.is_dir());
    
    // Try to open it with wrong passwords
    let mut p = start_program(&["open"], tmp_dir.path());
    p.write_line("Hjalp mii! Have forgotten the password. How reset it?");
    assert!(&p.stderr("Could not decrypt the vault. Is the password correct?\n").unwrap());
    p.write_line("All my files are lost! Is there anything to do?");
    assert!(&p.stderr("Could not decrypt the vault. Is the password correct?\n").unwrap());
    p.write_line("Shiiit I'm screwed! 😭😭");
    assert!(&p.stderr("Could not decrypt the vault. Is the password correct?\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));
}


/// Try invalid input arguments
#[test]
fn try_invalid_arguments() {
    let tmp_dir = tempdir().unwrap();
    let vault_path = tmp_dir.path().join(VAULT_DEFAULT_NAME);

    create_dir_and_fill_it(&vault_path);

    let mut p = start_program(&[], tmp_dir.path());
    assert!(&p.stderr("Invalid input: Not enough arguments.\n\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));

    let mut p = start_program(&["open", "this", "now", "please"], tmp_dir.path());
    assert!(&p.stderr("Invalid input: Too many arguments.\n\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));

    let mut p = start_program(&["lolwut"], tmp_dir.path());
    assert!(&p.stderr("Invalid input: Second argument must be some of following:\n").unwrap());
    assert!(&p.stderr("    'open', 'close', '--help', '--about'\n\n").unwrap());
    assert!(&p.stderr(YOU_SCREVED_INPUT).unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));

    let mut p = start_program(&["close", "my_vault"], tmp_dir.path());
    assert!(&p.stderr("Invalid input: Use command like:\n").unwrap());
    assert!(&p.stderr("    $ cryptdir close my_directory my_crypted_vault\n").unwrap());
    assert!(&p.stderr("or if directory is named as 'open_vault', then you can just use\n").unwrap());
    assert!(&p.stderr("    $ cryptdir close\n").unwrap());
    assert!(&p.stderr("See --help for more.\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));

    let mut p = start_program(&["--help"], tmp_dir.path());
    assert!(&p.stdout(YOU_SCREVED_INPUT).unwrap());
    assert_eq!(p.poll_exit_status(), Some(0));

    let mut p = start_program(&["--about"], tmp_dir.path());
    assert!(&p.stdout(ABOUT_TEXT).unwrap());
    assert_eq!(p.poll_exit_status(), Some(0));

    let mut p = start_program(&["close"], tmp_dir.path());
    assert!(&p.stdout("Give passphrase: ").unwrap());
    p.write_line(PASSWORD);
    assert!(&p.stdout("Confirm it: ").unwrap());
    p.write_line(PASSWORD);
    assert_eq!(p.poll_exit_status(), Some(0));


    let mut p = start_program(&["open", "this", "now"], tmp_dir.path());
    assert!(&p.stderr("Invalid input: Use command like:\n").unwrap());
    assert!(&p.stderr("    $ cryptdir open my_crypted_vault\n").unwrap());
    assert!(&p.stderr("or if vault is named as 'encrypted_vault', then you can just use\n").unwrap());
    assert!(&p.stderr("    $ cryptdir open\n").unwrap());
    assert!(&p.stderr("See --help for more.\n").unwrap());
    assert_eq!(p.poll_exit_status(), Some(2));
}