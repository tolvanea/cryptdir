/// Unit tests, integration tests and helper functions.
mod unit_tests;
mod integration_tests;
mod run_self;

use std::io::{Write, Read};
use std::{fs, fs::File};
use std::path::{Path};

/// Creates some directory with content
fn create_dir_and_fill_it<T:AsRef<Path>>(dir_path: T) {
    let dir_path = dir_path.as_ref();
    let subdir_path = dir_path.join("a directory");
    fs::create_dir_all(dir_path).unwrap();
    fs::create_dir_all(&subdir_path).unwrap();

    let mut file = File::create(dir_path.join("foo.txt")).unwrap();
    file.write_all(b"File one").unwrap();

    let mut file = File::create(dir_path.join("bar.txt")).unwrap();
    file.write_all(b"File two").unwrap();

    let mut file = File::create(subdir_path.join("large_file")).unwrap();
    let large_data = (0..1_000_000).map(|i| i as u8).collect::<Vec<_>>();
    file.write_all(&large_data).unwrap();
}

fn read_created_dir<T:AsRef<Path>>(dir_path: T) {
    let dir_path = dir_path.as_ref();
    let subdir_path = dir_path.join("a directory");

    let mut file = File::open(dir_path.join("foo.txt")).unwrap();
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();
    assert_eq!(content, "File one");

    let mut file = File::open(dir_path.join("bar.txt")).unwrap();
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();
    assert_eq!(content, "File two");

    let mut file = File::open(subdir_path.join("large_file")).unwrap();
    let mut content = Vec::new();
    file.read_to_end(&mut content).unwrap();
    assert!(content.into_iter().eq((0..1_000_000).map(|i| i as u8)));
}
