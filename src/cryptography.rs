use block_modes::{BlockMode, Cbc, block_padding::Pkcs7};
// Software implementation of AES
use aes_soft::Aes128 as Aes128_soft;
// X86 instruction implementation of AES. Runtime check will be made
#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
use aesni::Aes128 as Aes128_instr;

use pbkdf2::pbkdf2;
type Res<T> = anyhow::Result<T>;

/// Take passphrase, and hash it to two 16-length byte arrays. These arrays can
/// be used as a key and IV for AES encryption. This is known as "Key
/// stretching". This hashing takes ~100ms of CPU time, so brute force attacks
/// are harder even with short passphrase space.
pub fn hash_passphrase_to_bytes(passphrase: &str) -> ([u8; 16], [u8; 16]) {
    use std::convert::TryInto;
    use hmac::Hmac;
    use sha2::Sha256;
    let mut input = String::from_utf8(vec![96 as u8;32]).unwrap();
    input.push_str(passphrase);
    // The constant salt is used, which does not matter now
    let salt = [97u8; 16];
    let mut hash = [0u8; 32];
    pbkdf2::<Hmac<Sha256>>(passphrase.as_bytes(), &salt, 10000, &mut hash);
    return (hash[..16].try_into().unwrap(), hash[16..32].try_into().unwrap());
}

/// Encrypt bytes with AES
pub fn encrypt_with_passphrase(passphrase: &str, data: &[u8]) -> Vec<u8> {
    let (key, iv) = hash_passphrase_to_bytes(passphrase);

    // Run time check whether AES instructions are available
    #[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
    {
        if std::is_x86_feature_detected!("aes") && std::is_x86_feature_detected!("sse3") {
            let cipher = Cbc::<Aes128_instr,Pkcs7>::new_var(&key, &iv).unwrap();
            let encrypted = cipher.encrypt_vec(data);
            return encrypted;
        }
        else {
            let cipher = Cbc::<Aes128_soft, Pkcs7>::new_var(&key, &iv).unwrap();
            let encrypted = cipher.encrypt_vec(data);
            return encrypted;
        }
    }
    // Compilation for non-x86 devices (e.g. ARM)
    #[cfg(not(any(target_arch = "x86", target_arch = "x86_64")))]
    {
        let cipher = Cbc::<Aes128_soft, Pkcs7>::new_var(&key, &iv).unwrap();
        let encrypted = cipher.encrypt_vec(data);
        return encrypted;
    }
}

/// Decrypt bytes with AES
pub fn decrypt_with_passphrase(passphrase: &str, crypted_data: &[u8]) -> Res<Vec<u8>> {
    let (key, iv) = hash_passphrase_to_bytes(passphrase);
        // Run time check whether AES instructions are available
    #[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
    {
        if std::is_x86_feature_detected!("aes") && std::is_x86_feature_detected!("sse3") {
            let cipher = Cbc::<Aes128_instr,Pkcs7>::new_var(&key, &iv).unwrap();
            let data = cipher.decrypt_vec(crypted_data)?;
            return Ok(data);
        }
        else {
            let cipher = Cbc::<Aes128_soft, Pkcs7>::new_var(&key, &iv).unwrap();
            let data = cipher.decrypt_vec(crypted_data)?;
            return Ok(data);
        }
    }
    // Compilation for non-x86 devices (e.g. ARM)
    #[cfg(not(any(target_arch = "x86", target_arch = "x86_64")))]
    {
        let cipher = Cbc::<Aes128_soft, Pkcs7>::new_var(&key, &iv).unwrap();
        let data = cipher.decrypt_vec(crypted_data)?;
        return Ok(data);
    }
}
