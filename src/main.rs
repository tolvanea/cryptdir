
//! Encrypt and decrypt directories easily.
//!
//! # Usage case 1: Default names "open_vault" and "encrypted_vault"
//! 0. Create directory named "open_vault" and put secret data in it
//! 1. Navigate current directory to be parent of "open_vault"
//! 2. Encrypt directory "open_vault" to file named "encrypted_vault":
//!     * `$ ./cryptdir close`
//! 3. Data is now secured. Time can pass.
//! 4. Decrypt encrypted file named "encrypted_vault" to directory "open_vault":
//!     * `$ ./cryptdir open`
//! 5. Program is left running in terminal. You can make now modifications to "open_vault". Secure vault by typing 'close'
//!
//! # Usage case 2: Custom directory name and vault name:
//! 1. Close directory to encrypted vault
//!     * `$ cryptdir close my_dir my_vault`
//! 2. Open encrypted vault to directory
//!     * `$ cryptdir open my_vault`
//!
//!
//! Tested only on POSIX / Linux. Windows sucks anyways.
//!

// TODO?
// Release requirement that everything should fit into ram
//
// As current implementations decrypts directory to hard drive, unencrypted data may be recoverable
// after closing the vault. Therefore it would be good to implement automatic mounting to ramdisk
// such as `/dev/shm/` or preferably to `ramfs`. More sources to look for my self:
// https://unix.stackexchange.com/questions/26364/how-can-i-create-a-tmpfs-as-a-regular-non-root-user
// https://www.thegeekstuff.com/2008/11/overview-of-ramfs-and-tmpfs-on-linux/
// https://crates.io/crates/libmount
use std::{fs, fs::File};
use std::io::{Read, stdin, Write, stdout, stderr};
use std::path::Path;

use cryptography::{decrypt_with_passphrase, encrypt_with_passphrase};
use user_input::{ask_passphrase, process_args, UserInput};
use archive_directory::{unpack_archive_from_bytes_to_dir, archive_dir_to_bytes};

mod archive_directory;
mod cryptography;
mod user_input;
#[cfg(test)]
mod tests;

type Res<T> = anyhow::Result<T>;

const ORIG_DIR_NAME_INFO: &str = "some_temporary_file_that_hopefully_does_not_clash_with_name.txt";

/// Encrypt directory to vault
///
/// # Arguments
///
/// * `passphrase`  - Password
/// * `src`         - Path to directory that will be encrypted
/// * `dst`         - Path to encrypted file that will be created
fn archive_and_encrypt<T, U>(passphrase: &str, src: T, dst: U) -> Res<()>
    where T: AsRef<Path>, U: AsRef<Path>,
{
    let (vault, encrypted_vault)  = (src.as_ref(), dst.as_ref());
    let vault_name = vault.to_str().unwrap();
    // Save information about original name of directory
    let tmp_info_file = vault.join(ORIG_DIR_NAME_INFO);
    if tmp_info_file.exists(){
        panic!("Error: Could not create temporal file '{}'\n\
                in vault as it already exists.",
               ORIG_DIR_NAME_INFO);
    }
    let mut info_file = File::create(&tmp_info_file)?;
    info_file.write_all(vault_name.as_bytes())?;
    info_file.flush()?;

    // Pack directory to one file
    let bytes = match archive_dir_to_bytes(vault){
        Ok(b) => b,
        Err(_) => {
            fs::remove_file(&tmp_info_file).unwrap();
            panic!("Internal error: Could not archive the directory '{}'.", vault_name);
        },
    };

    // Encrypt
    let encrypted = encrypt_with_passphrase(passphrase, bytes.as_slice());

    let mut encrypted_file = File::create(encrypted_vault)
        .unwrap_or_else(
            |_| panic!("Error: Could not create file '{}'", encrypted_vault.to_str().unwrap())
        );
    encrypted_file.write_all(encrypted.as_slice())?;
    return Ok(());
}

/// Ask password, then decrypt and unpack archive.
/// These all are in one function because the separation would be messy.
///
/// # Arguments
///
/// * `encrypted_vault`     - Path to vault that will be opened
fn authenticate_and_decrypt_and_unarchive<T: AsRef<Path>>(encrypted_vault: T) -> Res<(String, String)>
{
    let encrypted_vault  = encrypted_vault.as_ref();

    //Decrypt
    let mut buf = Vec::new();
    let mut file = match File::open(&encrypted_vault){
        Ok(f) => f,
        Err(e) => {
            match e.kind() {
                std::io::ErrorKind::NotFound => {
                    eprintln!(
                        "Error: Encrypted vault with name '{}' not found in current directory.",
                        encrypted_vault.to_str().unwrap()
                    );
                }
                _ => panic!(e)
            }
            std::process::exit(2);
        },
    };
    let _len = file.read_to_end(&mut buf).expect("Could not read vault.");
    let mut counter = 0;
    let (passphrase, clear_data) = loop {
        let passphrase = ask_passphrase("Give passphrase: ");
        match decrypt_with_passphrase(&passphrase, buf.as_slice()) {
            Ok(d) => break (passphrase, d),
            Err(_) => {
                counter += 1;
                eprintln!("Could not decrypt the vault. Is the password correct?");
                if counter >= 3 {
                    std::process::exit(2);
                }
            },
        };
    };
    // Unpack
    let tmp_vault_name = "temporal_vault_name_which_hopefully_does_not_clash_names";
    let opened_vault = Path::new(&tmp_vault_name);
    unpack_archive_from_bytes_to_dir(clear_data.as_slice(), opened_vault)
        .expect("Error: Could not unpack archive somewhy.");

    // Fetch information of original directory name and rename
    let mut file = File::open(opened_vault.join(ORIG_DIR_NAME_INFO))?;
    let mut original_name = String::new();
    file.read_to_string(&mut original_name)?;
    drop(file);
    let original_name = Path::new(&original_name);
    if original_name.is_dir() {
        eprint!(
            "Error: Output vault '{}' already exists. Overwrite?\n[Y/N] ",
            original_name.to_str().unwrap()
        );
        stderr().flush()?;
        let mut s= String::new();
        stdin().read_line(&mut s).expect("Could not read input somewhy.");
        let s = s.trim();
        if s == "Y" || s == "y" {
            fs::remove_dir_all(original_name)?;
        }
        else {
            fs::remove_dir_all(opened_vault)?;
            eprintln!("Not opening vault.");
            std::process::exit(2);
        }
    }
    fs::remove_file(opened_vault.join(ORIG_DIR_NAME_INFO))?;
    std::fs::rename(opened_vault, &original_name).expect("Could not rename vault.");
    return Ok((passphrase, original_name.to_str().unwrap().to_owned()));
}

fn run() -> Res<()> {

    match process_args() {
        UserInput::Open{encrypted_vault} => {
            let (passphrase, original_name)
                = authenticate_and_decrypt_and_unarchive(&encrypted_vault)?;
            fs::remove_file(&encrypted_vault)?;

            // Now user does his/her business

            // Wait user input and close vault
            print!{"Close vault by typing 'close'. Alternatively, vault can be left open\n\
            either by typing 'leave open' or by terminating this program.\n> "};
            stdout().flush()?;
            loop {
                let mut s= String::new();
                match stdin().read_line(&mut s) {
                    Ok(_) => (),
                    Err(_) => {
                        eprintln!("Could not read input somewhy.");
                        continue;
                    }
                }
                let s = s.trim();
                if (s == "leave open") || (s == "exit") {
                    println!{"Vault was left open. You can close it by './cryptdir close'"};
                    return Ok(());
                }
                if (s == "close") || (s == "Close") || (s == "CLOSE") {
                    break;
                }
                eprintln!("Command not recognized. Valid commands are: 'close' and 'leave open'")
            };
            archive_and_encrypt(&passphrase, &original_name, &encrypted_vault)?;

            // Remove opened vault
            fs::remove_dir_all(&original_name)?;

        },
        UserInput::Close{vault, encrypted_vault} => {
            // Initial error checks
            let vault_name = vault.to_str().unwrap();
            if !vault.is_dir(){
                eprintln!("Error: Directory '{}' does not exist in current directory.", vault_name);
                std::process::exit(2);
            }
            if encrypted_vault.exists(){
                eprintln!("Error: Path '{}' already exists.", encrypted_vault.to_str().unwrap());
                std::process::exit(2);
            }

            // Encrypt
            let passphrase1 = ask_passphrase("Give passphrase: ");
            let passphrase2 = ask_passphrase("Confirm it: ");
            if passphrase1 != passphrase2 {
                eprintln!("Error: Passwords does not match.");
                std::process::exit(2);
            }

            archive_and_encrypt(&passphrase1, &vault, &encrypted_vault)?;

            // Remove old directory
            fs::remove_dir_all(vault)?;
        }
    };

    return Ok(());
}


fn main() -> Res<()> {
    run()?;
    return Ok(());
}
