use std::path::{Path, PathBuf};

pub const YOU_SCREVED_INPUT: &str =
"cryptdir - Encrypt and decrypt directories easily
--------------------------------------------------

Use case 1:
    Encrypt directory 'my_dir' to encrypted vault 'enc'
        $ ./cryptdir close my_dir enc

    Decrypt vault 'enc' to directory with 'my_dir'
        $ ./cryptdir open enc

    Program is left running in terminal so that modifications can be made.
    Close vault by typing 'close'.


Use case 2: Using default names
    There is shorthand command with use of default names. If directory is named as
    'open_vault', then encryption to 'encrypted_vault' is just
        $ ./cryptdir close

    Decrypt vault 'encrypted_vault' to direcotry 'open_vault':
        $ ./cryptdir open


Print this help:
    $ cryptdir --help

Security notices and more information:
    $ cryptdir --about";


pub const ABOUT_TEXT: &str =
"cryptdir - Encrypt and decrypt directories easily
--------------------------------------------------
How encryption is implemented:
    Directory is first compressed to tar-archive, then it is encrypted with AES-128.

Known 'features' (e.g. flaws)
    - Vault is opened as normal directory. Closing a vault just encrypts it to
      file and removes original directory. The removed data may be recoverable
      from hard drive.

    - Directory must fit fully in RAM.

    - Encryption/decrypting is slow for large directory sizes. With AESni instruction
      support, 1 GB takes few second, and without it, it takes half a minute.

    - This software is not written nor audited by anyone with cryptography
      background. The author has implemented no cryptography himself, but uses external
      libraries for that. These libraries are widely used and well trusted. So this
      program is more like bundle of existing tools with simple user interface.

Source Code:
https://gitlab.com/tolvanea/cryptdir
Alpi Tolvanen
Licence: MIT
";
pub const VAULT_DEFAULT_NAME: &str = "open_vault";
pub const ENCRYPTED_DEFAULT_NAME: &str = "encrypted_vault";

/// Arguments given to program
#[derive(Debug)]
pub enum UserInput {
    Open{encrypted_vault: PathBuf},
    Close{vault: PathBuf, encrypted_vault: PathBuf},
}

/// Read and extract arguments given to program.
pub fn process_args() -> UserInput {
    let args: Vec<String> = std::env::args().collect();
    let args_len = args.len();
    if args_len <= 1 {
        eprintln!("Invalid input: Not enough arguments.\n");
        eprintln!("{}", YOU_SCREVED_INPUT);
        std::process::exit(2);
    }
    else if args_len > 4 {
        eprintln!("Invalid input: Too many arguments.\n");
        eprintln!("{}", YOU_SCREVED_INPUT);
        std::process::exit(2);
    }

    // Now (args_len == 2) or (args_len == 3) or (args_len == 4)

    if (&args[1] == "--help") || (&args[1] == "help") || (&args[1] == "-h") {
        println!("{}", YOU_SCREVED_INPUT);
        std::process::exit(0);
    }
    else if (&args[1] == "--about") || (&args[1] == "about") {
        println!("{}", ABOUT_TEXT);
        std::process::exit(0);
    }
    else if !(&args[1] == "open" || &args[1] == "close") {
        eprintln!("Invalid input: Second argument must be some of following:");
        eprintln!("    'open', 'close', '--help', '--about'\n");
        eprintln!("{}", YOU_SCREVED_INPUT);
        std::process::exit(2);
    }

    if &args[1] == "open" {
        if args_len == 2 {
            return UserInput::Open {encrypted_vault: Path::new(ENCRYPTED_DEFAULT_NAME).to_path_buf()};
        } else if args_len == 3 {
            return UserInput::Open { encrypted_vault: Path::new(&args[2]).to_path_buf() };
        } else {
            eprintln!(
"Invalid input: Use command like:
    $ cryptdir open my_crypted_vault
or if vault is named as 'encrypted_vault', then you can just use
    $ cryptdir open
See --help for more.");
            std::process::exit(2);
        }
    }
    else /*Close*/ {
        if args_len == 2 {
            return UserInput::Close{
                vault: Path::new(VAULT_DEFAULT_NAME).to_path_buf(),
                encrypted_vault: Path::new(ENCRYPTED_DEFAULT_NAME).to_path_buf(),
            }
        } else if args_len == 4 {
            return UserInput::Close{
                vault: Path::new(&args[2]).to_path_buf(),
                encrypted_vault: Path::new(&args[3]).to_path_buf(),
            }
        } else {
            eprintln!(
"Invalid input: Use command like:
    $ cryptdir close my_directory my_crypted_vault
or if directory is named as 'open_vault', then you can just use
    $ cryptdir close
See --help for more."
            );
            std::process::exit(2);
        }
    }
}


/// Ask passoword so that typed letters are not written in promt.
pub fn ask_passphrase(prompt: &str) -> String {
    // I could use 'read_password_from_tty' (it is safer), but I can not test it
    // with integration tests.
    use rpassword::prompt_password_stdout;
    let pass = prompt_password_stdout(prompt).unwrap();
    return pass;
}
